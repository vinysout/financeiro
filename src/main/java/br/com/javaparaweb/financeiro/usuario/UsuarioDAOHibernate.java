package br.com.javaparaweb.financeiro.usuario;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;


public class UsuarioDAOHibernate implements UsuarioDAO {
	
	private Session session;
	
	public void setSession(Session session) {
		this.session = session;
	}
	
	@Override
	public void salvar(Usuario usuario) {
		this.session.save(usuario);
	}

	@Override
	public void atualizar(Usuario usuario) {
		if(usuario.getPermissao() == null || usuario.getPermissao().size() == 0) {
			Usuario usuarioPermissao = this.carregar(usuario.getCodigo());
			usuario.setPermissao(usuarioPermissao.getPermissao());
			this.session.evict(usuarioPermissao);
		}
		this.session.update(usuario);
	}

	@Override
	public void excluir(Usuario usuario) {
		this.session.delete(usuario);
	}

	@Override
	public Usuario carregar(Integer codigo) {
		return (Usuario) this.session.get(Usuario.class, codigo);
	}
	@Override
	public Usuario buscaPorLogin(String login) {
		return (Usuario) this.session.createQuery(
				"SELECT u FROM Usuario u WHERE u.login = :login")
				.setParameter("login", login)
				.getSingleResult();
	}
	@Override
	public List<Usuario> listar() {		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Usuario> cr = cb.createQuery(Usuario.class);
		Root<Usuario> root = cr.from(Usuario.class);
		cr.select(root);

		Query<Usuario> query = session.createQuery(cr);
		List<Usuario> results = query.getResultList();
		return results;
				
	}

}
