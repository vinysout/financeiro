package br.com.javaparaweb.financeiro.categoria;

import java.util.List;

import org.hibernate.Session;

import br.com.javaparaweb.financeiro.usuario.Usuario;

public class CategoriaDAOHibernate implements CategoriaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public Categoria salvar(Categoria categoria) {
		Categoria merged = (Categoria) this.session.merge(categoria);
		this.session.flush();
		this.session.clear();
		return merged;
	}

	@Override
	public void excluir(Categoria categoria) {
		categoria = (Categoria) this.carregar(categoria.getCodigo());
		this.session.delete(categoria);
		this.session.flush();
		this.session.clear();
	}

	@Override
	public Categoria carregar(Integer categoria) {
		return (Categoria) this.session.get(Categoria.class, categoria);
	}

	@Override
	public List<Categoria> listar(Usuario usuario) {		
		List<Categoria> result = this.session.createQuery(
				"SELECT c FROM Categoria c WHERE c.pai is null AND c.usuario = " + usuario.getCodigo(), Categoria.class)
				.getResultList();
		return result;
	}
}
