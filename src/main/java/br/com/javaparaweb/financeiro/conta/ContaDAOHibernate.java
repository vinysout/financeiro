package br.com.javaparaweb.financeiro.conta;

import java.util.List;


import org.hibernate.Session;
import br.com.javaparaweb.financeiro.usuario.Usuario;

public class ContaDAOHibernate implements ContaDAO {
	
	private Session session;
	public void setSession(Session session) {
		this.session = session;
	}
	
	@Override
	public void salvar(Conta conta) {
		// TODO Auto-generated method stub
		this.session.saveOrUpdate(conta);
	}

	@Override
	public void excluir(Conta conta) {
		// TODO Auto-generated method stub
		this.session.delete(conta);
	}

	@Override
	public Conta carregar(Integer conta) {
		// TODO Auto-generated method stub
		return (Conta) this.session.get(Conta.class, conta);
	}

	@Override
	public List<Conta> listar(Usuario usuario) {
		@SuppressWarnings("unchecked")
		List<Conta> result = this.session.createQuery(
				"SELECT c FROM Conta c WHERE c.usuario LIKE :usuario")
				.setParameter("usuario", usuario)
				.getResultList();
		return result;
	}
	@Override
	public Conta buscarFavorita(Usuario usuario) {
		return (Conta) this.session.createQuery(
				"SELECT c FROM Conta c WHERE c.usuario LIKE :usuario AND c.favorita = true")
				.setParameter("usuario", usuario)
				.getSingleResult();
	}

}
