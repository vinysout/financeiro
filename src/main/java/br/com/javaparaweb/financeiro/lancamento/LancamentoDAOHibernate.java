package br.com.javaparaweb.financeiro.lancamento;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import br.com.javaparaweb.financeiro.conta.Conta;

public class LancamentoDAOHibernate implements LancamentoDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	public void salvar(Lancamento lancamento) {
		this.session.saveOrUpdate(lancamento);
	}

	public void excluir(Lancamento lancamento) {
		this.session.delete(lancamento);
	}

	public Lancamento carregar(Integer lancamento) {
		return (Lancamento) this.session.get(Lancamento.class, lancamento);
	}

	@Override
	public List<Lancamento> listar(Conta conta, Date dataInicio, Date dataFim) {

		if (dataInicio != null && dataFim != null) {
			return this.session.createQuery(
				"SELECT l FROM Lancamento l WHERE l.conta = :conta AND l.data BETWEEN :dataInicio AND :dataFim ORDER BY l.data", Lancamento.class)
				.setParameter("conta", conta)
				.setParameter("dataInicio", dataInicio)
				.setParameter("dataFim", dataFim)					
				.getResultList();
			
		} else if (dataInicio != null) {
			return this.session.createQuery(
				"SELECT l FROM Lancamento l WHERE l.conta = :conta AND l.data >= :dataInicio ORDER BY l.data", Lancamento.class)
				.setParameter("conta", conta)
				.setParameter("dataInicio", dataInicio)
				.getResultList();			
		} else{
			return this.session.createQuery(
				"SELECT l FROM Lancamento l WHERE l.conta = :conta AND l.data <= :dataFim ORDER BY l.data", Lancamento.class)
				.setParameter("conta", conta)
				.setParameter("dataFim", dataFim)
				.getResultList();		
		}
		//ATUALIZADO(DIFERENTE DO LIVRO)
	}

	public float saldo(Conta conta, Date data) {
		
		BigDecimal saldo = (BigDecimal) this.session.createQuery(
				"SELECT SUM(l.valor * c.fator) FROM Lancamento l, Categoria c WHERE l.categoria = c.codigo AND l.conta = :conta AND l.data <= :data")
			.setParameter("conta", conta)
			.setParameter("data", data)
			.getSingleResult();
		
		if (saldo != null) {
			return saldo.floatValue();
		}
		return 0f;
		//ATUALIZADO(DIFERENTE DO LIVRO)
	}
}
